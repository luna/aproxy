# aproxy

Activity Pub Reverse Proxy Framework

this is a collection of OpenResty scripts that provide different functionality
on top of ActivityPub implementations.

## Installation

### the fuck is openresty

Get OpenResty: http://openresty.org/en/

For those not in the know, OpenResty is a "soft fork" of the god NGINX.
It adds various modules but the most important of them is Lua scripting
(provided by another god, LuaJIT)

It is an effective replacement to your NGINX installation, but you can have
them coexisting (say, NGINX on port 80, OpenResty being reverse proxied by
NGINX on port 8069, though I wouldn't recommend it in production environments).

### how does it work

aproxy has two "hooks" into openresty:
 - initialization of the lua vm
 - callback for every incoming request

initialization will run validation of your configuration file and check
if it is valid. if it is not you will see logs emitted about what failed

when a request comes in, the scripts declared in the aproxy config file will
be executed sequentially (TODO: ensure order on conf file is chain order).

each script has two types of callbacks: init and request

init callbacks are called when initializing the request, so that the script
may do some conversion or transform the config data into a more performant
in-memory structure.

request callbacks are called on each request, as directed by the main script.
scripts define which paths they want to attach to (via PCRE regex), and so
they can do their own filtering.

look at `scripts/webfinger_allowlist.lua` for an example of how this looks
in a simple form.

### actually installing aproxy

- get openresty installed
  - keep in mind that the specifics of configuring openresty for a double reverse proxy setup aren't included here.
  - instructions here are for aproxy's setup in an existing openresty installation

```sh
mkdir /opt
git clone https://gitdab.com/luna/aproxy

cd aproxy
mkdir /etc/aproxy
cp ./conf.lua /etc/aproxy/conf.lua

# keep in mind the default configuration will lead to your users not being discovered at all,
# it is provided as an example for you to modify.
$EDITOR /etc/aproxy/conf.lua
```

#### now to configuring openresty to use aproxy

Say, you already have your reverse proxy block to your instance as such:
```nginx
http {
    server {
        server_name example.com;
        location / {
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_set_header Host $http_host;
            proxy_pass http://localhost:4000;
        }
    }
}
```

You need to do the following:
 - Configure OpenResty package path so that it can call aproxy.
 - insert aproxy hooks for initialization and for callbacks on every request

It'll look something like this if you use a single `location /` block:

```nginx
# set this to 'on' after you have tested that it actually works.
# once you do that, performance will be increased
# while the friction to quickly debug aproxy will also be increased
lua_code_cache off;
lua_package_path '/opt/?.lua;/opt/aproxy/?.lua;;';
init_by_lua_block {
  require("aproxy.main").init()
}

http {
    server {
        server_name example.com;
        location / {
            access_by_lua_block {
                require("aproxy.main").access()
            }

            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_set_header Host $http_host;
            proxy_pass http://localhost:4000;
        }
    }
}
```

## Running the test suite

requires luarocks, luajit, and nothing else.

```sh
make testdeps
eval (luarocks-5.1 path --bin)
make test
```
