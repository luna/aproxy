local ctx = require('ctx')
local config = require('config')
require('util')

ctx:loadFromConfig(config.loadConfigFile())

return {
    init=function ()
        -- validate config and print out errors
        config.loadConfigFile({validate = true})
    end,
    access=function()
        ctx:onRequest()
    end
}
