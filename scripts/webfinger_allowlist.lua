local function webfingerInit(cfg)
    local accounts_set = {}
    for _, account in ipairs(cfg.accounts) do
        accounts_set["acct:" .. account] = true
    end
    return accounts_set
end

local function webfingerCallback(cfg, accounts_set)
    local args, err = ngx.req.get_uri_args()
    if err == "truncated" then
      return 400, 'uri args too long'
    end

    local resource = args['resource']
    if accounts_set[resource] then
        return nil
    else
        return 404, "Couldn't find user"
    end
end

return {
    name='WebfingerAllowlist',
    author='luna@l4.pm',
    title='Webfinger Allowlist',
    description=[[
    Prevent unnecessary DB load by discarding requests to users that we know
    won't exist.

    Useful for small instances.
    ]],
    version=1,
    init=webfingerInit,
    callbacks = {
        ['/.well-known/webfinger'] = webfingerCallback
    },
    config={
        ['accounts'] = {
            type='list',
            schema={
                type='string',
                description='ap id'
            },
            description = 'list of account ids (in email@domain form) to pass through to AP'
        }
    },
}
