lu = require('luaunit')
local rex = require('rex_pcre2')
require('util')

function createNgx()
    local ngx = {
        status = nil
    }

    local function mockedThing(self, property)
        return function(value)
            self['_'..property] = value
        end
    end

    ngx.say = mockedThing(ngx, "say")
    ngx.exit = mockedThing(ngx, "exit")

    ngx.log = function (_, msg)
        print(msg)
    end

    -- only hold data here
    ngx.var = {}

    -- request params api
    ngx.req = {}

    ngx.req.get_uri_args = function ()
        return ngx._uri_args
    end

    ngx.req.set_uri_args = function (val)
        ngx._uri_args = val
    end

    -- regex api
    ngx.re = {}
    ngx.re.match = rex.match
    ngx.re.search = rex.find

    return ngx
end

function resetNgx()
    ngx = createNgx()
end
teardownNgx = resetNgx

function setupFakeRequest(path, options)
    ngx.var.uri = path
    if options.params then
        ngx.req.set_uri_args(options.params)
    end
end

local ctx = require('ctx')
local config = require('config')
function setupTest(module_require_path, input_config)
    resetNgx()
    local module = require(module_require_path)

    local schema_errors = config.validateSchema(module.config, input_config)
    local count = table.pprint(schema_errors)
    lu.assertIs(count, 0)

    local state = module.init(input_config)
    ctx.compiled_chain = {
        {module, input_config, state}
    }
    return module
end


function onRequest()
    ctx:setWantedScripts()
    local context = require('ctx')
    do
        context:onRequest()
    end
end

require('tests.webfinger_allowlist')
require('tests.schema_validation')
os.exit(lu.LuaUnit.run())
