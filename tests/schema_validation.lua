TestSchemaValidator = {}

local config = require('config')

function TestSchemaValidator:testBasicFields()
    local errors = config.validateSchema({a={type='string'}}, {a='test'})
    lu.assertIs(table.len(errors), 0)
    local errors = config.validateSchema({a={type='number'}}, {a=123})
    lu.assertIs(table.len(errors), 0)
    local errors = config.validateSchema({a={type='string'}}, {a=123})
    lu.assertIs(table.len(errors), 1)
end

function TestSchemaValidator:testList()
    local errors = config.validateSchema({a={type='list', schema={type='number'}}}, {a={1,2,3}})
    lu.assertIs(table.len(errors), 0)

    local errors = config.validateSchema({a={type='list', schema={type='number'}}}, {a={1,2,3,'asd'}})
    lu.assertIs(table.len(errors), 1)
end

function TestSchemaValidator:testTable()
    local TEST_SCHEMA = {
            a={
                type='table',
                schema={
                    b={
                        type='number'
                    }
                }
            }
    }

    local errors = config.validateSchema(
        TEST_SCHEMA,
        {a=
            {b=2}
        }
    )
    lu.assertIs(table.len(errors), 0)

    local errors = config.validateSchema(
        TEST_SCHEMA,
        {a=
            {b='sex'}
        }
    )
    lu.assertIs(table.len(errors), 1)
end
