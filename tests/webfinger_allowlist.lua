TestWebfinger = {}

function TestWebfinger:setup()
    self.mod = setupTest('scripts.webfinger_allowlist', {accounts = {'correct@example.org'}})
end

local WEBFINGER_PATH = '/.well-known/webfinger'

function TestWebfinger:testCorrectAccount()
    setupFakeRequest(WEBFINGER_PATH, { params = {resource = 'acct:correct@example.org'} })
    onRequest()
    lu.assertIs(ngx.status, nil)
end

function TestWebfinger:testWrongAccount()
    setupFakeRequest(WEBFINGER_PATH, { params = {resource = 'acct:wrong@example.org'} })
    onRequest()
    lu.assertIs(ngx.status, 404)
end

function TestWebfinger:teardown()
    teardownNgx()
end
