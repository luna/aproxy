function table.len(t)
  local count = 0
  for _ in pairs(t) do count = count + 1 end
  return count
end

function table.pprint(t, options_in, ident_in, total_count_in)
    local ident = ident_in or 0
    local total_count = total_count_in or 0

    local options = options_in or {}
    local print_function = options.call or print
    if type(t) == 'table' then
        local count = 0
        for k, v in pairs(t) do
            print_function(string.rep('\t', ident) .. k)
            count = count + 1
            total_count = table.pprint(v, options, ident + 1, total_count)
        end
        if count == 0 then
            print_function('{}')
        end
    else
        print_function(string.rep('\t', ident) .. tostring(t))
        total_count = total_count + 1
    end
    return total_count
end

function table.readonly(t)
   return setmetatable({}, {
     __index = t,
     __newindex = function ()
                    error("Attempt to modify read-only table")
                  end,
     __metatable = false
   });
end

function string.split(inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={}
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        table.insert(t, str)
    end
    return t
end


